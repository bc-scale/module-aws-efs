resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnet" "default" {
  vpc_id            = data.aws_vpc.default.id
  availability_zone = data.aws_availability_zones.available.names[0]
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "example" {
  statement {
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    actions = [
      "elasticfilesystem:ClientMount",
      "elasticfilesystem:ClientWrite",
      "elasticfilesystem:ClientRootAccess",
    ]

    resources = [module.one_zone_storage_policy.arn]
  }
}

resource "aws_security_group" "example" {
  vpc_id = data.aws_vpc.default.id
  tags = {
    Name = "tftest-example-${random_string.this.result}"
  }
}

module "one_zone_storage_policy" {
  source = "../../"

  tags = {
    Name = "tftest${random_string.this.result}"
  }

  one_zone_storage       = true
  availability_zone_name = data.aws_availability_zones.available.names[0]
  subnet_ids             = [data.aws_subnet.default.id]

  name                = "tftest${random_string.this.result}"
  security_group_name = "tftest${random_string.this.result}"

  kms_key_alias_name = "alias/tftest/${random_string.this.result}"
  kms_key_name       = "tftest${random_string.this.result}"

  allowed_security_group_ids = [aws_security_group.example.id]

  enable_efs_backup_policy = true

  enable_efs_file_system_policy = true
  efs_file_system_policy_json   = data.aws_iam_policy_document.example.json

  lifecycle_policy = {
    transition_to_primary_storage_class = "AFTER_1_ACCESS"
  }

  ssm_parameter_enabled = false
}
