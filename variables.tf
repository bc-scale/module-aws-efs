#####
# Module
#####

variable "lifecycle_policy" {
  description = <<-DOCUMENTATION
   Lifecycle policy of the EFS
    * transition_to_ia                    (optional, string): Indicates how long it takes to transition files to the IA storage class. Valid values: `AFTER_7_DAYS`, `AFTER_14_DAYS`, `AFTER_30_DAYS`, `AFTER_60_DAYS`, or `AFTER_90_DAYS`.
    * transition_to_primary_storage_class (optional, string): Describes the policy used to transition a file from infrequent access storage to primary storage. Valid values: `AFTER_1_ACCESS`.
DOCUMENTATION
  type = object({
    transition_to_ia                    = optional(string)
    transition_to_primary_storage_class = optional(string)
  })
  default = null

  validation {
    condition = var.lifecycle_policy == null || (
      contains(["AFTER_7_DAYS", "AFTER_14_DAYS", "AFTER_30_DAYS", "AFTER_60_DAYS", "AFTER_90_DAYS"], coalesce(lookup(coalesce(var.lifecycle_policy, {}), "transition_to_ia", "AFTER_7_DAYS"), "AFTER_7_DAYS")) &&
      contains(["AFTER_1_ACCESS"], coalesce(lookup(coalesce(var.lifecycle_policy, {}), "transition_to_primary_storage_class", "AFTER_1_ACCESS"), "AFTER_1_ACCESS"))
    )
    error_message = "“var.lifecycle_policy” is invalid. Check the requirements in the variables.tf file."
  }
}

variable "tags" {
  description = "Tags to be shared among all resources of this module."
  default     = {}
}

variable "subnet_ids" {
  description = "IDs of the subnet where the EFS should be made available. If none are specified, it will be deployed in the default vpc but WITHOUT mount targets."
  default     = []
  type        = list(string)
}

#####
# EFS
#####

variable "name" {
  description = "Name of the EFS."
  default     = "efs"
}

variable "one_zone_storage" {
  description = "Whether or not to create a file system that uses `One Zone` storage classes"
  default     = false
}

variable "availability_zone_name" {
  description = "The AWS Availability Zone in which to create the file system. Used only if `var.one_zone_storage` is set to `true`"
  type        = string
  default     = null
}

variable "performance_mode" {
  description = "The file system performance mode. Can be either ”generalPurpose” or “maxIO”."
  default     = "generalPurpose"
}

variable "provisioned_throughput_in_mibps" {
  description = "The throughput, measured in MiB/s, that you want to provision for the file system. Only applicable with `throughput_mode` set to “provisioned”."
  default     = 0
}

variable "throughput_mode" {
  description = "Throughput mode for the file system. Valid values: ”bursting”, “provisioned”. When using `provisioned`, also set the `provisioned_throughput_in_mibps` variable."
  default     = "bursting"
}

variable "enable_efs_backup_policy" {
  description = "Enable EFS backup policy."
  default     = false
}

variable "enable_efs_file_system_policy" {
  description = "Enable EFS file system policy"
  default     = false
}

variable "efs_file_system_policy_bypass_policy_lockout_safety_check" {
  description = "A flag to indicate whether to bypass the `var.efs_file_system_policy` lockout safety check."
  default     = false
}

variable "efs_file_system_policy_json" {
  description = "The JSON formatted file system policy for the EFS file system. This will be ignore is `enable_efs_file_system_policy` is false and mandatorty if true"
  type        = string
  default     = null
}

variable "efs_tags" {
  description = "Tags specific for the EFS. Will be merged with `var.tags`."
  default     = {}
}

variable "efs_access_points" {
  description = <<-DOCUMENTATION
   A map of objects that represent the access points to add to the EFS.
   Each access_point object must have the format:
    * posix_user     (optional, object): Operating system user and group applied to all file system requests made using the access point.
    * root_directory (optional, object): Directory on the Amazon EFS file system that the access point provides access to.
  posix_user must have the format:
    * gid  (required, number): POSIX group ID used for all file system operations using this access point.
    * secondary_gids (optional, list(number)): Secondary POSIX group IDs used for all file system operations using this access point.
    * uid (required, number): POSIX user ID used for all file system operations using this access point.
  root_directory must have the format:
    * path (optional, string):  Path on the EFS file system to expose as the root directory to NFS clients using the access point to access the EFS file system. A path can have up to four subdirectories. If the specified path does not exist, you are required to provide `creation_info`.
    * creation_info (optional, object): POSIX IDs and permissions to apply to the access point's Root Directory.
  creation_info must have the format:
    * owner_gid (required, number): POSIX group ID to apply to the `root_directory`.
    * owner_uid (required, number): POSIX user ID to apply to the `root_directory`.
    * permissions (required, number): POSIX permissions to apply to the `root_directory`, in the format of an octal number representing the file's mode bits.
DOCUMENTATION
  default     = {}
  type = map(object({
    posix_user = optional(object({
      gid            = number
      secondary_gids = optional(list(number))
      uid            = number
    }))
    root_directory = optional(object({
      path = optional(string)
      creation_info = optional(object({
        owner_gid   = number
        owner_uid   = number
        permissions = string
      }))
    }))
  }))

  validation {
    condition = var.efs_access_points == {} || (
      !contains([
        for k, content in var.efs_access_points : (
          (content.posix_user == null ? true : (
            content.posix_user.gid >= 0 &&
            content.posix_user.uid >= 0 &&
            (content.posix_user.secondary_gids == null ? true : !contains([for gid in content.posix_user.secondary_gids : gid > 0], false))
          )) &&
          (content.root_directory == null ? true : (
            (content.root_directory.path == null ? true : can(regex("^(\\/|(\\/[^$#<>;`|&?{}^*/\n]+){1,4})$", content.root_directory.path))) &&
            (content.root_directory.creation_info == null ? true : (
              content.root_directory.creation_info.owner_uid >= 0 &&
              content.root_directory.creation_info.owner_gid >= 0 &&
              can(regex("^[0-7]{3,4}$", content.root_directory.creation_info.permissions)))
            )
          ))
        )
      ], false)
    )
    error_message = "One or more 'var.efs_access_points' are invalid. Check the requirements in the variables.tf file."
  }
}

variable "efs_access_point_tags" {
  description = "Tags specific to the Access Points. Will be merged with `var.tags`."
  default     = {}
}

#####
# KMS
#####

variable "kms_key_arn" {
  description = "ARN of the KMS key to be used to encrypt the EFS. Should be specified when `kms_key_create` is `false`."
  default     = ""
}

variable "kms_key_create" {
  description = "Whether or not to create the KMS key for the EFS."
  default     = true
}

variable "kms_key_alias_name" {
  description = "Name of the KMS key alias to be used to encrypt the EFS."
  default     = "alias/efs"
}

variable "kms_key_name" {
  description = "Name of the KMS key to be used to encrypt the EFS."
  default     = "efs"
}

variable "kms_tags" {
  description = "Tags specific for the KMS key for the EFS. Will be merged with `var.tags`."
  default     = {}
}

#####
# SSM Parameter
#####

variable "ssm_parameter_enabled" {
  description = "Whether or not to create SSM Parameters containing EFS metadata."
  default     = false
}

variable "ssm_parameter_prefix" {
  description = "Prefix for the SSM Parameters created by this module. It should an absolute path without trailing slash (e.g /my/example/path)."
  default     = "/efs/module/default"
}

variable "ssm_parameter_tags" {
  description = "Tags specific for the SSM Parameters for the EFS. Will be merged with tags."
  default     = {}
}

#####
# Security group
#####

variable "security_group_ids" {
  description = "List of additional security group IDs for the EFS mount targets. Required if no `allowed_cidrs` nor `allowed_security_group_ids` is provided."
  default     = []

  validation {
    condition = length(var.security_group_ids) == 0 || (
      !contains([
        for cidr in var.security_group_ids : (
          can(regex("^sg-([a-z0-9]{8}|[a-z0-9]{17})$", cidr))
        )
      ], false)
    )
    error_message = "One or more of the “var.security_group_ids” does not match '^sg-([a-z0-9]{8}|[a-z0-9]{17})$'."
  }
}

variable "security_group_name" {
  description = "Name of the security group to be used by the EFS mount targets. Security group will be create ONLY IF `var.allowed_cidrs` or `var.allowed_security_group_ids` is NOT an empty list."
  default     = "efs"
}

variable "allowed_cidrs" {
  description = "CIDRs allowed to access the EFS. By specifying this value, the module will create a new security group to attach to the EFS mount targets - with these CIDRS as targets - in addition to `var.security_group_ids`."
  default     = []

  validation {
    condition = length(var.allowed_cidrs) == 0 || (
      !contains([
        for cidr in var.allowed_cidrs : (
          can(regex("^([0-9]{1,3}\\.){3}[0-9]{1,3}\\/([0-9]|[1-2][0-9]|3[0-2])$", cidr))
        )
      ], false)
    )
    error_message = "One or more of the “var.allowed_cidrs” does not match '^([0-9]{1,3}\\.){3}[0-9]{1,3}\\/([0-9]|[1-2][0-9]|3[0-2])$'."
  }
}

variable "allowed_security_group_ids" {
  description = "ID of the security groups allowed to access the EFS. By specifying this value, the module will create a new security group to attach to the EFS mount targets - with these security group IDs as targets - in addition to `var.security_group_ids`."
  default     = []

  validation {
    condition = length(var.allowed_security_group_ids) == 0 || (
      !contains([
        for cidr in var.allowed_security_group_ids : (
          can(regex("^sg-([a-z0-9]{8}|[a-z0-9]{17})$", cidr))
        )
      ], false)
    )
    error_message = "One or more of the “var.allowed_security_group_ids” does not match '^sg-([a-z0-9]{8}|[a-z0-9]{17})$'."
  }
}

variable "security_group_tags" {
  description = "Additional tags specific for the security group for the EFS mount targets. Will be merged with `var.tags`."
  type        = map(string)
  default     = {}
}
